#include "./src/background.c"
#include "./src/fade.c"
#include "./src/game_over_screen.c"
#include "./src/game_screen.c"
#include "./src/intro_screen.c"
#include "./src/main.c"
#include "./src/math.c"
#include "./src/pipe.c"
#include "./src/player.c"
#include "./src/score.c"
#include "./src/screens.c"
#include "./src/shake.c"
#include "./src/splash_screen.c"
